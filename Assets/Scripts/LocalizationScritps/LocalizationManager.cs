﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using LitJson;

public class LocalizationManager : MonoBehaviour {

	string path;
	string JsonString;
	JsonData jsonItemData;
	public string Langue;

	public static LocalizationManager Single;

	void Awake(){
		AutoLocalization ();
		LangLoader ();

		Single = this;
	}

	void Start(){
		
	}

	void AutoLocalization(){//Определение языка устнонвленого на устройстве
		if (Application.systemLanguage == SystemLanguage.Belarusian || Application.systemLanguage == SystemLanguage.Russian || Application.systemLanguage == SystemLanguage.Ukrainian) {
			Langue = "ru_RU.json";
			//Langue = "en_EN.json";
		}
		if (Application.systemLanguage == SystemLanguage.English) {
			Langue = "en_EN.json";
		}
		if (Application.systemLanguage != SystemLanguage.Belarusian && Application.systemLanguage != SystemLanguage.Russian && Application.systemLanguage != SystemLanguage.Ukrainian) {
			Langue = "en_EN.json";
		}
	}

	void LangLoader(){//Загрузка языкового покета для игры
		#if UNITY_ANDROID && !UNITY_EDITOR
		path = Application.streamingAssetsPath +"/LocalizationData/" + Langue;
		WWW reader = new WWW(path);
		while (!reader.isDone) {}
		JsonString = reader.text;
		jsonItemData = JsonMapper.ToObject (JsonString);
		#endif
		#if UNITY_EDITOR
		path = Application.streamingAssetsPath +"/LocalizationData/" + Langue;
		JsonString = File.ReadAllText (path);
		jsonItemData = JsonMapper.ToObject (JsonString);
		#endif

	}

	public string GetWord(string Key){//Запрос на слово
		return(jsonItemData [Key].ToString());
	}
}


