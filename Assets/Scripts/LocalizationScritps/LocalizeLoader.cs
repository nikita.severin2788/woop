﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalizeLoader : MonoBehaviour {

	public string KeyWord;

	void Start(){

		if (GetComponent<Text> () != null && KeyWord != null) {
			GetComponent<Text> ().text = LocalizationManager.Single.GetWord (KeyWord);
		}
	}
}
