﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderScript : MonoBehaviour {

	public GameObject Slider;
	public Text SliderText;
	public bool ModeFade;

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (ModeFade) {
			ScorePanelFadeIn ();
		} else {
			ScorePanelFadeOut ();
		}
	}

	void ScorePanelFadeIn(){
		Slider.GetComponent<RectTransform> ().localPosition = Vector2.Lerp (Slider.GetComponent<RectTransform> ().localPosition, new Vector3 (0, 0, 0), 10 * Time.deltaTime);
	}
	void ScorePanelFadeOut(){
		Slider.GetComponent<RectTransform> ().localPosition = Vector2.Lerp (Slider.GetComponent<RectTransform> ().localPosition, new Vector3 (40, 0, 0), 10 * Time.deltaTime);
	}

	public void SetText(int Score){
		SliderText.text = Score.ToString() + " i ";
	}
}
