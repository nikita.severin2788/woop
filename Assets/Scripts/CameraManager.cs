﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {

	public Vector3 CameraPositon;
	public float CameraSpeed;

	public static CameraManager Instance;

	void Awake(){
		Instance = this;
	}

	void Start(){
		CameraPositon = transform.position;
	}
		
	void Update(){
		transform.position = Vector3.Lerp (transform.position, CameraPositon, CameraSpeed * Time.deltaTime);
	}

	public void SetPoisiontX(float XPos){
		CameraPositon.x = XPos;
	}

}
