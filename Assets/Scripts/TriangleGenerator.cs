﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriangleGenerator : MonoBehaviour {

	public int TriangleCount;
	public Vector3 SetGeneratePostion;
	public GameObject Triangle;

	public static TriangleGenerator Instance;

	void Awake(){
		Instance = this;
	}

	void Start(){
		StartCoroutine ("Generate");
	}

	IEnumerator Generate(){

		while(TriangleCount < 20) {
			SetGeneratePostion = new Vector3 (transform.position.x + Random.Range (-8f, 5f), transform.position.y, transform.position.z+ Random.Range (-2f, 2f));
			Instantiate (Triangle, SetGeneratePostion, Quaternion.Euler (0, 0, 0));
			TriangleCount++;
			yield return new WaitForSeconds (0.5f);
		}
	}

}
