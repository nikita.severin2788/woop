﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

	public int Score;
	public Text ScoreText;

	bool firstPlatform;

	[Header("UIScenes")]
	public GameObject GameUI;
	public GameObject Menu;
	public GameObject StartGameUI;

	[Header("LoseMenu")]
	public Text BestScore;
	public Text YouScore;

	public static UIManager Instance;

	void Awake(){
		Instance = this;
	}


	void Update(){
		if (ScoreText.color.a != 255) {
			ScoreText.color = new Color (ScoreText.color.r, ScoreText.color.g, ScoreText.color.b, Mathf.MoveTowards (ScoreText.color.a, 255, 4 * Time.deltaTime));
		}
	}

	public void AddScore(){
		if (firstPlatform == true) {
			ScoreText.color = new Color(ScoreText.color.r,ScoreText.color.g,ScoreText.color.b,0);

			Score++;
			ScoreText.text = Score.ToString ();

		}
		firstPlatform = true;
	}

	public void DeadMenu(){
		GameUI.SetActive (false);
		Menu.SetActive (true);
		BestScore.text = GameManager.Instance.LoadBestScore ().ToString();
		YouScore.text = GameManager.Instance.ThisScore.ToString ();
	}

	public void StartGame(){
		StartGameUI.SetActive (false);
		GameUI.SetActive (true);
	}
}
