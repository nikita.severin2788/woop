﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ScreenController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler{


	public void OnPointerDown(PointerEventData ped){
		PlayerController.Instace.OnDownToScreen = true;
	}

	public void OnPointerUp (PointerEventData ped){
		PlayerController.Instace.OnDownToScreen = false;
		PlayerController.Instace.ForceClear ();
	}
}
