﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlatformManager : MonoBehaviour {


	public Color StartColor;
	public Color EndColor;
	public float SpeedChangeColor;
	public float ChangeColor;
	public float TorwardSpeed = 1;
	public float FadeSpeed = 1;
	public bool StartFade = false;

	public int PlatformId;
	public GameObject BestScoreLine;

	[Header("AudioClip")]
	public AudioSource source;
	public AudioClip[] clip = new AudioClip[4]; 
	int RandomLand;
	//
	bool OneAdd = false;

	[Header("OtherElements")]
	public GameObject ScoreSlider;
	public GameObject InstSlider;

	void Start(){
		transform.localScale = new Vector3 (50,50, TorwardSpeed = Random.Range (100f, 300f));
		TorwardSpeed = TorwardSpeed / 500;

		if (PlatformId == GameManager.Instance.LoadBestScore()) {
			Instantiate (BestScoreLine, new Vector3 (transform.position.x, transform.position.y, transform.position.z + 0.5f), Quaternion.Euler (-180, 0, 0));
			InstSlider = Instantiate(ScoreSlider,new Vector3(transform.position.x + -1.372f, transform.position.y + -0.42f, transform.position.z + 0.501f), Quaternion.Euler(0,0,0));
			InstSlider.GetComponent<SliderScript> ().SetText (GameManager.Instance.LoadBestScore ());
		}

	}


	void OnCollisionEnter(Collision col){
        Debug.Log("Caaalll!!!");
		if (col.collider.tag == "Player") {
			if (transform.position.x + 0.80f >= PlayerController.Instace.transform.position.x && transform.position.x - 0.80f <= PlayerController.Instace.transform.position.x) {
				PlayerController.Instace.SetToCenterPlatform (transform.position.x);
				//PlayerController.Instace.transform.GetComponent<Rigidbody> ().freezeRotation = true;
				PlayerController.Instace.transform.rotation = Quaternion.Euler(PlayerController.Instace.transform.rotation.x,PlayerController.Instace.transform.rotation.y,0);
				PlayerController.Instace.transform.GetComponent<Rigidbody> ().Sleep ();
				CameraManager.Instance.SetPoisiontX(transform.position.x);
				//
				if (OneAdd == false) {
					UIManager.Instance.AddScore ();
					GameManager.Instance.AddScore();
					OneAdd = true;
				}
				StartFade = true;
				RandomLand = Random.Range(0,4);
				source.PlayOneShot (clip [RandomLand], 0.5f);

				if (InstSlider != null) {
					InstSlider.GetComponent<SliderScript> ().ModeFade = true;
				}
			}
		}
	}
	void OnCollisionExit(Collision col){
		if (col.collider.tag == "Player") {
			StartFade = false;
		}
	}

	void Update(){
		if (StartFade) {
			transform.localScale = new Vector3 (50, 50, Mathf.MoveTowards (transform.localScale.z, 0, FadeSpeed * Time.deltaTime));
			transform.GetComponent<MeshRenderer> ().material.color = Color.Lerp (StartColor, EndColor, ChangeColor = Mathf.MoveTowards (ChangeColor, 1, TorwardSpeed * Time.deltaTime));
			if (transform.localScale.z < 3) {
				Destroy (gameObject);
			}
		}
		//
		if (PlayerController.Instace.transform.position.x >= transform.position.x + 10) {
			PlatformGenerator.Instance.PlatformCount--;
			if (InstSlider != null) {
				Destroy (InstSlider);
			}
			Destroy (gameObject);
		}
		//
		if (transform.localScale.z < 70 && InstSlider != null) {
			InstSlider.GetComponent<SliderScript> ().ModeFade = false;
		}
	}
}
