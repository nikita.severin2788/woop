﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
//using GoogleMobileAds.Api;

public class GameManager : MonoBehaviour {

	public static GameManager Instance;

	public int ThisScore;

  //  private BannerView bannerView;

    

	void Awake(){
		Instance = this;
	}

	void Start(){

   // #if UNITY_ANDROID
  //      string appId = "ca-app-pub-2511492677863672~9934300232";
  //  #endif
 
        //MobileAds.Initialize(appId);
        

        if (LoadCountDead () == null) {
			SaveCountDead (0);
		}
		ThisScore--;
        
    }

    void ShowAd(object a, EventArgs args)
    {
        print("Loaded");
        //bannerView.Show();
    }

    public void AddScore(){
		ThisScore++;
		if (ThisScore > LoadBestScore()) {
			SaveBestScore (ThisScore);
		}
	}

	public string LoadSoundMode(){
		Debug.Log ("LoadAudio: " + PlayerPrefs.GetString ("SoundMode"));
		return PlayerPrefs.GetString ("SoundMode");

	}
	public void SaveSoundMode(string Mode){
		PlayerPrefs.SetString ("SoundMode", Mode);
		Debug.Log ("SaveAudio: " + Mode);
	}

	public int LoadBestScore(){
		return PlayerPrefs.GetInt ("BestScore");
	}

	public void SaveBestScore(int Score){
		PlayerPrefs.SetInt ("BestScore",Score);
	}

	public void SaveCountDead(int Count){
		PlayerPrefs.SetInt ("CountDead", Count);
	}
	public int LoadCountDead(){
		return PlayerPrefs.GetInt ("CountDead");
	}

	public void Dead(){
		SaveCountDead (LoadCountDead() + 1);
		
		UIManager.Instance.DeadMenu ();
        if (LoadCountDead() % 1 == 0)
        {  
            SaveCountDead(0);
        }
       // #if UNITY_ANDROID
       // string adUnitId = "ca-app-pub-2511492677863672/7041310440";
       // #endif
        //bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);
        //AdRequest request = new AdRequest.Builder().AddTestDevice("EB32961D9042BE555A3A82B4ED055FDB").Build();
        //bannerView.LoadAd(request);

       // bannerView.OnAdLoaded += ShowAd;
    }

	public void Restart(){
        //bannerView.Hide();
        SceneManager.LoadScene (0);
	}
}
