﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour {

	public AudioSource MenuOst;
	public AudioSource MainThem;

	public AudioClip ClipMainThem;
	public AudioClip ClipMenuOst;

	public float SpeedChangeMusic;

	public static MusicManager Instance;

	void Awake(){
		Instance = this;
	}

	public void OffMenu(){
		StartCoroutine ("OffMenuOst");
	}

	public void OnMenu(){
		StartCoroutine ("OnMenuOst");
	}

	public void OffMain(){
		StartCoroutine ("OffMainOst");
	}

	public void OnMain(){
		StartCoroutine ("OnMainOst");
	}

	public void MenuToGame(){
		StartCoroutine ("OffMenuOst");
		StartCoroutine ("OnMainOst");
	}


	public IEnumerator OffMainOst(){
		while (MainThem.volume == 0.01f) {
			MainThem.volume = Mathf.MoveTowards (MainThem.volume, 0, SpeedChangeMusic * Time.deltaTime);
		}
		if (MainThem.volume <= 0.02f) {
			StopCoroutine ("OffMainOst");
		}
		yield return true;
	}

	public IEnumerator OnMainOst(){
		while (MainThem.volume == 0.99f) {
			MainThem.volume = Mathf.MoveTowards (MainThem.volume, 1, SpeedChangeMusic * Time.deltaTime);
		}
		if (MainThem.volume >= 0.95f) {
			StopCoroutine ("OnMainOst");
		}
		yield return true;
	}

	public IEnumerator OffMenuOst(){
		while (MenuOst.volume  == 0.01f) {
			MenuOst.volume = Mathf.MoveTowards (MenuOst.volume, 0, SpeedChangeMusic * Time.deltaTime);
		}
		if (MenuOst.volume <= 0.02f) {
			StopCoroutine ("OffMenuOst");
		}
		yield return true;
	}

	public IEnumerator OnMenuOst(){
		while (MenuOst.volume == 0.99f) {
			MenuOst.volume = Mathf.MoveTowards (MenuOst.volume, 1, SpeedChangeMusic * Time.deltaTime);
		}
		if (MenuOst.volume >= 0.95f) {
			StopCoroutine ("OnMenuOst");
		}
		yield return true;
	}

	void Update () {


	}
}
