﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShadowAnimation : MonoBehaviour {


	[Header("PlayMode")]
	public bool OnePlay;
	[Header("Settings")]
	public Vector2 MaxShadow;
	public Vector2 MinShadow;
	public float SpeedFade;
	bool ShadowMoving;
	Shadow ShadowScript;

	void Start(){
		ShadowScript = GetComponent<Shadow> ();
	}

	void Update () {
		if (ShadowScript.effectDistance == MinShadow) {
			ShadowMoving = true;
		}
		if(ShadowScript.effectDistance == MaxShadow){
			ShadowMoving = false;
		}
		if (ShadowMoving) {
			ShadowScript.effectDistance = Vector2.MoveTowards (ShadowScript.effectDistance, MaxShadow, SpeedFade * Time.deltaTime);
		} else {
			if(OnePlay == false)
			ShadowScript.effectDistance = Vector2.MoveTowards (ShadowScript.effectDistance, MinShadow, SpeedFade * Time.deltaTime);
		}

	}
}
