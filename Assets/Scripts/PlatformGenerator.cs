﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformGenerator : MonoBehaviour {

	public int PlatformCount;
	public Vector3 SetGeneratePostion;
	public GameObject Platform;

	public static PlatformGenerator Instance;

	public int AllPlatformCount;

	void Awake(){
		Instance = this;
	}

	void Start(){
		AllPlatformCount--;
	}

	void Update(){

		if (PlatformCount < 10) {
			AllPlatformCount++;
			GameObject Obj = Instantiate (Platform, SetGeneratePostion, Quaternion.Euler (-90, 0, 0));
			Obj.GetComponent<PlatformManager> ().PlatformId = AllPlatformCount;
			SetGeneratePostion = new Vector3 (SetGeneratePostion.x + Random.Range (3f, 4f), 0, 0);
			PlatformCount++;
		}

	}

}
