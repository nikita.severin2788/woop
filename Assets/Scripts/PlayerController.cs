﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {


	Rigidbody Rb;

	[Header("ForceEditor")]
	public Vector3 ForceVectore;
	public float MaxForce;
	public float MinForce;
	public float SetForce;
	public float SpeedForce;

	[Header("Mods")]
	public bool ForceMode;
	public bool OnGround;
	public bool OnDownToScreen;

	[Header("StartCutScene")]
	public bool StartCutScene;
	public bool PositionMode;
	public Transform TopPoint;
	public Transform DownPoint;
	public float SpeedCutSceneAnimation;

	[Header("Output")]
	public float ForceLineOut;
	public float OneProcent;
	public static PlayerController Instace;
	public GameObject ForceLine;

	[Header("AudioClip")]
	public AudioSource source;
	public AudioClip[] clip = new AudioClip[4]; 
	int RandomWoop;

	bool OnDead = false;

	void Awake(){
		Instace = this;
	}

	void Start(){
		Rb = GetComponent<Rigidbody> ();
		Rb.isKinematic = true;

	}


	public void StartGame(){
		StartCutScene = false;
		Rb.isKinematic = false;
		UIManager.Instance.StartGame ();
		//MusicManager.Instance.MenuToGame ();
	}

	public void SetToCenterPlatform(float PosX){
		transform.position = new Vector3 (PosX, transform.position.y, transform.position.z);
	}

	public void ForceClear(){
		ForceLineOut = 0;
		SetForce = 0;
		//ForceVectore = new Vector3(0,0,0);
		ForceLine.GetComponent<RectTransform> ().localScale = new Vector3 (0,ForceLine.GetComponent<RectTransform> ().localScale.y,ForceLine.GetComponent<RectTransform> ().localScale.z);
	}

	public void Jump(){
		if (OnGround) {
			//GetComponent<Rigidbody> ().freezeRotation = false;
			Rb.AddForce(ForceVectore,UnityEngine.ForceMode.Impulse);
			OnGround = false;
			RandomWoop = Random.Range(0,4);
			source.PlayOneShot (clip [RandomWoop], 0.5f);
		}
	}

	void Update(){

		if (StartCutScene == true) {

			if (transform.position.y == TopPoint.position.y) {
				PositionMode = false;
			}
			if (transform.position.y == DownPoint.position.y) {
				PositionMode = true;
			}

			if (PositionMode == true) {
				transform.position = Vector3.MoveTowards (transform.position, TopPoint.transform.position, SpeedCutSceneAnimation * Time.deltaTime);
			} else {
				transform.position = Vector3.MoveTowards (transform.position, DownPoint.transform.position, SpeedCutSceneAnimation * Time.deltaTime);
			}
		}

		if (OnDownToScreen) {
			//
			OneProcent = (MaxForce/100f);
			ForceLineOut = SetForce / OneProcent; 
			ForceLine.GetComponent<RectTransform> ().localScale = new Vector3 (ForceLineOut / 100,ForceLine.GetComponent<RectTransform> ().localScale.y,ForceLine.GetComponent<RectTransform> ().localScale.z);
			//
			if (ForceMode == false) {
				SetForce = Mathf.MoveTowards (SetForce, MaxForce, SpeedForce * Time.deltaTime);
			} else {
				SetForce = Mathf.MoveTowards (SetForce, MinForce, SpeedForce * Time.deltaTime);
			}
			//
			if (SetForce == MaxForce) {
				ForceMode = true;
			}
			if (SetForce == MinForce) {
				ForceMode = false;
			}
			//
			ForceVectore = new Vector3(SetForce,SetForce,0);
		}

		//
		if (transform.position.y < -1) {
			if (OnDead == false) {
				GameManager.Instance.Dead ();
				UIManager.Instance.DeadMenu ();
				OnDead = true;
			}
		}
		if (transform.position.y < -10) {
			GetComponent<Rigidbody> ().isKinematic = true;
		}
	}

	void OnCollisionStay(Collision col){
		if (col.collider.tag == "Platform") {
			OnGround = true;
		}
	}

	void OnCollisionExit(Collision col){
		if (col.collider.tag == "Platform") {
			OnGround = false;
		}
	}
}
