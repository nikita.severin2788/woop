﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriangleBackgroundControler : MonoBehaviour {


	public float RotationSpeedX;
	public float RotationSpeedY;
	public float RotationSpeedZ;

	public float SpeedUp;
	public float UpYPos;

	void Start(){

		/*RotationSpeedX = Random.Range (-90, 90);
		RotationSpeedY = Random.Range (-90, 90);
		RotationSpeedZ = Random.Range (-90, 90);*/
		SpeedUp = Random.Range (0.1f,0.3f);
	}

	void Update(){

		/*transform.Rotate (Vector3.left * RotationSpeedX * Time.deltaTime);
		transform.Rotate (Vector3.up * RotationSpeedY * Time.deltaTime);
		transform.Rotate (Vector3.back * RotationSpeedZ * Time.deltaTime);*/

		transform.position = Vector3.Lerp (transform.position, new Vector3 (transform.position.x, UpYPos, transform.position.z), SpeedUp * Time.deltaTime);

		if (TriangleGenerator.Instance.transform.position.y + UpYPos - 1 <= transform.position.y) {
			Destroy (gameObject);
			TriangleGenerator.Instance.TriangleCount--;
		}
	}
}
