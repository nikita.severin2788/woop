﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SoundButton : MonoBehaviour, IPointerDownHandler {

	public AudioListener AL;
	public Image ImageSound;
	public Sprite OnSound;
	public Sprite OffSound;
	public bool SoundMode;

	void Start(){
		
		if (GameManager.Instance.LoadSoundMode () != "On"){
			if (GameManager.Instance.LoadSoundMode () != "Off") {
				GameManager.Instance.SaveSoundMode ("On");
				SoundMode = true;
			}
		}

		if (GameManager.Instance.LoadSoundMode () == "On") {
			SoundMode = true;
		}
		if (GameManager.Instance.LoadSoundMode () == "Off") {
			SoundMode = false;
		}
		if (SoundMode) {
			AL.enabled = true;
		} else{
			AL.enabled = false;
		}

	}


	public void OnPointerDown(PointerEventData ped){
		SoundMode = !SoundMode;

		if (SoundMode) {
			ImageSound.sprite = OnSound;
			AL.enabled = true;
			GameManager.Instance.SaveSoundMode ("On");
		} else{
			ImageSound.sprite = OffSound;
			AL.enabled = false;
			GameManager.Instance.SaveSoundMode ("Off");
		}
	}

}
